# ICFCA19 Experiments

This project provides raw data and results of the experiments ran for ICFCA 2019.

## Raw datasets
The datasets are extracted from [DBpedia](https://wiki.dbpedia.org/). They are provided as raw data (RDF triples). They are provided in the archive `raw_data.zip`.

## Rules

For each dataset, two `csv` files (one for redescriptions and one for association rules) provide the rules obtained along with their evaluations :

* eval : Evaluation from the experts (0/1)
* query_LHS_named : Category defined
* query_RHS_named : Descriptors of the query
* acc : Jaccard coefficient (redescriptions) / confidence (association rules)
* pval : p-value of the rule (for redescriptions only)
* card_E_ij : Number of objects satisfying query_LHS_named, query_RHS_named, both queries or none of them (for redescriptions only).

## Algorithms

Experiments were run with the algorithm `ReReMi` via [Siren](http://siren.gforge.inria.fr/main/)([paper](https://hal.archives-ouvertes.fr/hal-01399211)) and with the algorithm `Eclat` via [Coron](http://coron.loria.fr/site/index.php)([paper](http://coron.wdfiles.com/local--files/biblio%3Aconf/coron-demo-icfca10.pdf))